﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class NPC
{
    [SerializeField]
    Sprite _npcImage;
    [SerializeField]
    string _npcName;
    [SerializeField]
    List<HerbEffect> _askedPotions;
    [SerializeField]
    int _payment;
    [SerializeField]
    bool _hasBeenServed = false;

    public Sprite NpcImage
    {
        get
        {
            return _npcImage;
        }

        set
        {
            _npcImage = value;
        }
    }

    public string NpcName
    {
        get
        {
            return _npcName;
        }

        set
        {
            _npcName = value;
        }
    }

    public List<HerbEffect> AskedPotions
    {
        get
        {
            return _askedPotions;
        }

        set
        {
            _askedPotions = value;
        }
    }

    public int Payment
    {
        get
        {
            return _payment;
        }

        set
        {
            _payment = value;
        }
    }

    public bool HasBeenServed
    {
        get
        {
            return _hasBeenServed;
        }

        set
        {
            _hasBeenServed = value;
        }
    }
}
