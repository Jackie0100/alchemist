﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Potion : IInventoryItem
{
    [SerializeField]
    string _name;
    [SerializeField]
    int _amount;
    [SerializeField]
    bool _isMajor;
    [SerializeField]
    bool _hasBenIdentified;
    [SerializeField]
    Sprite _spriteImage;
    [SerializeField]
    HerbEffect _herbEffect;
    [SerializeField]
    bool _hasPlaceboEffect;
    [SerializeField]
    HerbData[] _herbs;

    public Sprite SpriteImage
    {
        get
        {
            return _spriteImage;
        }

        set
        {
            _spriteImage = value;
        }
    }

    public string Name
    {
        get
        {
            return _name;
        }

        set
        {
            _name = value;
        }
    }

    public HerbEffect HerbEffect
    {
        get
        {
            return _herbEffect;
        }

        set
        {
            _herbEffect = value;
        }
    }

    public bool IsMajor
    {
        get
        {
            return _isMajor;
        }

        set
        {
            _isMajor = value;
        }
    }

    public bool HasBenIdentified
    {
        get
        {
            return _hasBenIdentified;
        }

        set
        {
            _hasBenIdentified = value;
        }
    }

    public int Amount
    {
        get
        {
            return _amount;
        }

        set
        {
            _amount = value;
        }
    }

    public string ToolTipText
    {
        get
        {
            return (HasBenIdentified ? Name : "Unidentified Potion");
        }
        set
        {
        }
    }

    public bool HasPlaceboEffect
    {
        get
        {
            return _hasPlaceboEffect;
        }

        set
        {
            _hasPlaceboEffect = value;
        }
    }

    public HerbData[] Herbs
    {
        get
        {
            return _herbs;
        }

        set
        {
            _herbs = value;
        }
    }
}
