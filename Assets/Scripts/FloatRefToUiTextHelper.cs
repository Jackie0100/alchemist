﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class FloatRefToUiTextHelper : MonoBehaviour
{
    [SerializeField]
    FloatRef _value;
    [SerializeField]
    string _prefix;
    [SerializeField]
    string _postfix;
    [SerializeField]
    bool _convertToTime;
    Text _text;


    private void Start()
    {
        _text = GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {
        if (_convertToTime)
        {
            int m = (int)_value.Value % 60;
            int h = (int)(_value.Value / 60) % 24;

            _text.text = _prefix + h.ToString("D2") + ":" + m.ToString("D2") + _postfix;
        }
        else
        {
            _text.text = _prefix + _value.Value.ToString() + _postfix;
        }
    }
}
