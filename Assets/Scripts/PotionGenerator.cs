﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.UI;

public class PotionGenerator : MonoBehaviour
{
    public static PotionGenerator Instance { get; set; }

    [SerializeField]
    HerbData[] _herbs;
    [SerializeField]
    HerbEffect[] _herbEffects;
    [SerializeField]
    Sprite[] _potionSprites;
    [SerializeField]
    string[] _neutralPotionNames;
    [SerializeField]
    Potion[,] _potions;
    [SerializeField]
    Sprite[] _npcSprites;
    [SerializeField]
    string[] _npcNames;

    [SerializeField]
    HerbInventory _herbInventory;
    [SerializeField]
    PotionInventory _potionInventory;
    [SerializeField]
    ItemInventory _itemInventory;
    [SerializeField]
    FloatRef _day;
    [SerializeField]
    FloatRef _time;
    [SerializeField]
    FloatRef _money;
    [SerializeField]
    FloatRef _reputation;

    [SerializeField]
    GameObject _gameOverScreen;
    [SerializeField]
    Text _gameOverText;
    [SerializeField]
    GameObject _victoryScreen;

    [SerializeField]
    Sprite _playerImage;

    Dictionary<int, List<NPC>> _npcTestHistory = new Dictionary<int, List<NPC>>();
    Dictionary<int, List<NPC>> _npcSellHistory = new Dictionary<int, List<NPC>>();

    public int RandomSellNpcHashSeed { get; set; }
    public int RandomTestNpcHashSeed { get; set; }


    System.Random rng = new System.Random();

    int[] counter;

    bool _hasGenerated = false;

    private void Awake()
    {
        Instance = this;
        RandomSellNpcHashSeed = rng.Next(int.MinValue, int.MaxValue - 1000);
        RandomTestNpcHashSeed = rng.Next(int.MinValue, int.MaxValue - 1000);
    }

    private void Start()
    {
        _playerImage = _npcSprites[rng.Next(0, _npcSprites.Length)];
        /*var temp = _npcSprites.ToList();
        temp.Remove(_playerImage);
        _npcSprites = temp.ToArray();*/

        _reputation.Value = 50;
        _money.Value = 500;
        _time.Value = 420;
        _herbInventory.HerbList = new List<HerbData>();
        _itemInventory.ItemList = new List<HerbData>();
        _potionInventory.PotionList = new List<Potion>();
        
        GenerateEffects();
    }

    bool _hasCollected = false;
    bool _haswon = false;
    private void Update()
    {
        _day.Value = Mathf.Floor(_time.Value / 1440) + 1;
        if (_day.Value % 7 == 0)
        {
            if (_hasCollected)
                return;

            _hasCollected = true;

            if (_money.Value >= 2500)
            {
                SpeechArea.Instance.SetSpeech("Tax Collector", "Hey there alchemist, I'm here to collect the weekly tax of 2500 gold, Thank you for your continued help to the kingdom!");
                _reputation.Value += 10;
                _reputation.Value = Mathf.Clamp(_reputation.Value, 0, 100);
                _money.Value -= 2500;
            }
            else
            {
                SpeechArea.Instance.SetSpeech("Tax Collector", "Hey there alchemist, I'm here to collect the weekly tax of 2500 gold, WHAT! You can't pay your tax? I'm taking you the dungeon in this instant!");
                _gameOverScreen.SetActive(true);
                _gameOverText.text = "You were unable to pay your taxes, and the king has closed off your shop for good! You've now been locked up in jail and will need to serve in the mine until your debt has been paid off!";
            }
        }
        else
        {
            _hasCollected = false;
        }

        if (_reputation.Value >= 100)
        {
            if (!_haswon)
            {
                _victoryScreen.SetActive(true);
                _haswon = true;
            }
        }
        if (_reputation.Value <= 0)
        {
            _gameOverScreen.SetActive(true);
            _gameOverText.text = "Your reputation have gone completely down, people are now thinking you're a fraud and a witch! People are protesting and are trying to chase you out of the city with pitchfork. Maybe it was just pure hate, or maybe you sold people snake oil?";
        }
    }

    [Button]
    public void GenerateEffects()
    {
        if (_hasGenerated)
            return;

        int index = 0;
        do
        {
            _potionSprites = _potionSprites.OrderBy(x => rng.Next()).ToArray();
            _herbs = _herbs.OrderBy(x => rng.Next()).ToArray();
            _neutralPotionNames = _neutralPotionNames.OrderBy(x => rng.Next()).ToArray();


            foreach (HerbEffect he in _herbEffects)
            {
                he.TimesUsed = 0;
            }

            foreach (HerbData hd in _herbs)
            {
                hd.HasDiscoveredMinorEffect1 = false;
                hd.HasDiscoveredMinorEffect2 = false;
                hd.HasDiscoveredMinorEffect3 = false;
                hd.HasDiscoveredMajorEffect = false;
                hd.Amount = 0;
            }

            List<int> _herbEffectsIndexes = new List<int>();

            for (int i = 0; i < _herbs.Length; i++)
            {
                _herbEffectsIndexes.Add(i % _herbEffects.Length);
            }
            _herbEffectsIndexes = _herbEffectsIndexes.OrderBy(x => rng.Next()).ToList();

            index = 0;
            foreach (var herb in _herbs)
            {
                herb.MajorEffect = _herbEffects.ElementAt(_herbEffectsIndexes[index++]);
            }

            _herbEffectsIndexes = new List<int>();

            for (int i = 0; i < _herbs.Length * 3; i++)
            {
                _herbEffectsIndexes.Add(i % _herbEffects.Length);
            }
            _herbEffectsIndexes = _herbEffectsIndexes.OrderBy(x => rng.Next()).ToList();

            index = 0;
            foreach (var herb in _herbs)
            {
                herb.MinorEffects = new List<HerbEffect>();
                for (int i = 0; i < 3; i++)
                {
                    if (herb.MajorEffect != _herbEffects.ElementAt(_herbEffectsIndexes[index]) && !herb.MinorEffects.Contains(_herbEffects.ElementAt(_herbEffectsIndexes[index])) && herb.MajorEffect != _herbEffects.ElementAt(_herbEffectsIndexes[index]).OppesiteEffect && !herb.MinorEffects.Contains(_herbEffects.ElementAt(_herbEffectsIndexes[index]).OppesiteEffect))
                    {
                        herb.MinorEffects.Add(_herbEffects.ElementAt(_herbEffectsIndexes[index]));
                        _herbEffects.ElementAt(_herbEffectsIndexes[index]).TimesUsed++;
                        index++;
                    }
                }
            }

            foreach (var herb in _herbs.Where(x => x.MinorEffects.Count < 3))
            {
                foreach (HerbEffect he in _herbEffects.Where(x => x.TimesUsed < 3))
                {
                    if (herb.MajorEffect != he && !herb.MinorEffects.Contains(he) && herb.MajorEffect != he.OppesiteEffect && !herb.MinorEffects.Contains(he.OppesiteEffect))
                    {
                        herb.MinorEffects.Add(he);
                        he.TimesUsed++;
                        if (herb.MinorEffects.Count == 3)
                            break;
                    }
                }
                while (herb.MinorEffects.Count != 3)
                {
                    var temp = _herbEffects[rng.Next(0, _herbEffects.Length)];
                    if (herb.MajorEffect != temp && !herb.MinorEffects.Contains(temp) && herb.MajorEffect != temp.OppesiteEffect && !herb.MinorEffects.Contains(temp.OppesiteEffect))
                    {
                        herb.MinorEffects.Add(temp);
                        temp.TimesUsed++;
                    }
                }
            }
        } while (_herbEffects.All(x => x.TimesUsed != 3));

        _hasGenerated = true;

        _potions = new Potion[_herbs.Length, _herbs.Length];
        index = 0;
        int potspriteindex = 0;
        for (int i = 0; i < _herbs.Length; i++)
        {
            for (int j = i + 1; j < _herbs.Length; j++)
            {
                if (_herbs[i] == _herbs[j])
                    continue;

                if (_herbs[i].MajorEffect == _herbs[j].MajorEffect)
                {
                    _potions[i, j] = _potions[j, i] = new Potion() { Amount = 0, HerbEffect = _herbs[i].MajorEffect, Name = "Major Potion of " + _herbs[i].MajorEffect.Name, HasBenIdentified = false, IsMajor = true, SpriteImage = _potionSprites[potspriteindex % _potionSprites.Length], Herbs = new HerbData[] { _herbs[i], _herbs[j] } };
                    potspriteindex++;
                }
                else if (_herbs[i].MinorEffects.Contains(_herbs[j].MajorEffect))
                {
                    _potions[i, j] = _potions[j, i] = new Potion() { Amount = 0, HerbEffect = _herbs[j].MajorEffect, Name = "Potion of " + _herbs[j].MajorEffect.Name, HasBenIdentified = false, IsMajor = false, SpriteImage = _potionSprites[potspriteindex % _potionSprites.Length], Herbs = new HerbData[] { _herbs[i], _herbs[j] } };
                    potspriteindex++;

                }
                else if (_herbs[j].MinorEffects.Contains(_herbs[i].MajorEffect))
                {
                    _potions[i, j] = _potions[j, i] = new Potion() { Amount = 0, HerbEffect = _herbs[i].MajorEffect, Name = "Potion of " + _herbs[i].MajorEffect.Name, HasBenIdentified = false, IsMajor = false, SpriteImage = _potionSprites[potspriteindex % _potionSprites.Length], Herbs = new HerbData[] { _herbs[i], _herbs[j] } };
                    potspriteindex++;
                }
                else
                {
                    _potions[i, j] = _potions[j, i] = new Potion() { Amount = 0, HerbEffect = null, Name = _neutralPotionNames[index % _neutralPotionNames.Length], HasBenIdentified = false, IsMajor = false, SpriteImage = _potionSprites[potspriteindex % _potionSprites.Length], HasPlaceboEffect = (rng.Next(0, 100) >= 75), Herbs = new HerbData[] { _herbs[i], _herbs[j] } };
                    potspriteindex++;
                    index++;
                }
            }
        }
    }

    public List<HerbData> GetHerbs(int min, int max)
    {
        List<HerbData> retList = new List<HerbData>();
        int c = rng.Next(min, max);
        for (int i = 0; i < c; i++)
        {
            retList.Add(_herbs[rng.Next(0, _herbs.Length)]);
        }
        return retList;
    }

    public Potion GetCreatedPotion(HerbData ing1, HerbData ing2)
    {
        return _potions[_herbs.ToList().IndexOf(ing1), _herbs.ToList().IndexOf(ing2)];
    }

    public List<NPC> GenerateNPCs(bool issellingnpcs)
    {
        System.Random npcrng = null;

        if (issellingnpcs)
        {
            if (_npcSellHistory.ContainsKey(Mathf.RoundToInt(_day.Value)))
            {
                return _npcSellHistory[Mathf.RoundToInt(_day.Value)];
            }
        }
        else
        {
            if (_npcTestHistory.ContainsKey(Mathf.RoundToInt(_day.Value)))
            {
                return _npcTestHistory[Mathf.RoundToInt(_day.Value)];
            }
        }

        if (issellingnpcs)
        {
            npcrng = new System.Random(RandomSellNpcHashSeed + Mathf.RoundToInt(_day.Value));
        }
        else
        {
            npcrng = new System.Random(RandomTestNpcHashSeed + Mathf.RoundToInt(_day.Value));
        }

        List<NPC> npcs = new List<NPC>();

        npcs.Add(new NPC() { NpcName = "Me", NpcImage = _playerImage });

        int amount = npcrng.Next(2, 9);

        for (int i = 0; i < amount; i++)
        {
            var npc = new NPC() { NpcName = _npcNames[npcrng.Next(0, _npcNames.Length)], NpcImage = _npcSprites[npcrng.Next(0, _npcSprites.Length)], Payment = (int)((float)npcrng.Next(100, 1001) * (issellingnpcs ? 1 : 1.5f)), AskedPotions = new List<HerbEffect>() };

            int needamount = npcrng.Next(2, 6);

            for (int j = 0; j <= needamount; j++)
            {
                npc.AskedPotions.Add(_herbEffects[npcrng.Next(0, _herbEffects.Length)]);
            }

            npcs.Add(npc);
        }

        if (issellingnpcs)
        {
            _npcSellHistory[Mathf.RoundToInt(_day.Value)] = npcs;
        }
        else
        {
            return _npcTestHistory[Mathf.RoundToInt(_day.Value)] = npcs;
        }

        return npcs;
    }

    public void AddUsageTime(int amount)
    {
        _time.Value += amount;
    }
}
