﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIPotionInventory : MonoBehaviour
{
    [SerializeField]
    GameObject _itemUi;
    [SerializeField]
    PotionInventory _potionInventory;
    [SerializeField]
    PotionSellingUI _potionSellingUI;


    private void OnEnable()
    {
        foreach (var t in GetComponentsInChildren<UIInventoryItem>())
        {
            Destroy(t.gameObject);
        }

        GameObject go = null;
        foreach (var potion in _potionInventory.PotionList)
        {
            go = Instantiate(_itemUi, transform);
            go.GetComponent<UIInventoryItem>().Item = potion;
            go.GetComponent<UIInventoryItem>()._sellingUi = _potionSellingUI;

            go.GetComponent<Button>().onClick.AddListener(go.GetComponent<UIInventoryItem>().OnPotionItemClicked);
        }
    }
}
