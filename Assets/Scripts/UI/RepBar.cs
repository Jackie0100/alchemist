﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RepBar : MonoBehaviour
{
    [SerializeField]
    FloatRef _reputation;
    [SerializeField]
    Text _infoText;
    [SerializeField]
    Image _sliderBar;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        _infoText.text = "Reputation: " + Mathf.Round(_reputation.Value);
        _sliderBar.fillAmount = _reputation.Value / 100;
    }
}
