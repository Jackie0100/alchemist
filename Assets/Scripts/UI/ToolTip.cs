﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ToolTip : MonoBehaviour
{
    [SerializeField]
    Text _toolTipText;
    public static ToolTip Instance { get; set; }

    private void Awake()
    {
        Instance = this;
        gameObject.SetActive(false);
    }

    private void Update()
    {
        Vector2 pos;
        RectTransformUtility.ScreenPointToLocalPointInRectangle(transform as RectTransform, Input.mousePosition, Camera.main, out pos);
        transform.position = Input.mousePosition + (-Vector3.up * 50);
    }

    public void DisplayText(string text)
    {
        _toolTipText.text = text;
    }
}
