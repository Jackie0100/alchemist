﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BuyHerbsUI : MonoBehaviour
{
    [SerializeField]
    GameObject _itemUi;
    [SerializeField]
    HerbData[] _herbs;
    [SerializeField]
    HerbBuyingUI _buyingUi;


    private void OnEnable()
    {
        foreach (var t in GetComponentsInChildren<UIInventoryItem>())
        {
            Destroy(t.gameObject);
        }

        GameObject go = null;
        foreach (var herb in _herbs)
        {
            go = Instantiate(_itemUi, transform);
            go.GetComponent<UIInventoryItem>().Item = herb;
            go.GetComponent<UIInventoryItem>()._buyingUi = _buyingUi;
            go.GetComponent<UIInventoryItem>()._keepZeroes = true;
            go.GetComponent<Button>().onClick.AddListener(go.GetComponent<UIInventoryItem>().OnBuyHerbItemClicked);
        }
    }
}
