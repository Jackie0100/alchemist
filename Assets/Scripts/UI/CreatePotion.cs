﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(AudioSource))]
public class CreatePotion : MonoBehaviour
{
    [SerializeField]
    Image _ingredient1Sprite;
    [SerializeField]
    Image _ingredient2Sprite;
    [SerializeField]
    ItemTooltipHelper _itemTooltipHelper1;
    [SerializeField]
    ItemTooltipHelper _itemTooltipHelper2;
    [SerializeField]
    ItemTooltipHelper _itemTooltipHelper3;
    [SerializeField]
    Image _resultPotionSprite;
    [SerializeField]
    PotionInventory _potionInventory;
    [SerializeField]
    AudioClip _createPotionAudio;

    HerbData _ingredient1;
    HerbData _ingredient2;
    Potion _resultPotion;

    public HerbData Ingredient1
    {
        get
        {
            return _ingredient1;
        }

        set
        {
            if (value != null && value == _ingredient2)
                return;

            _ingredient1 = value;
            if (value == null)
            {
                _ingredient1Sprite.color = new Color(1, 1, 1, 0);
                _ingredient1Sprite.sprite = null;
                _itemTooltipHelper1._item = null;
            }
            else
            {
                _ingredient1Sprite.color = new Color(1, 1, 1, 1);
                _ingredient1Sprite.sprite = value.SpriteImage;
                _itemTooltipHelper1._item = value;
            }
            if (Ingredient2 != null && value != null)
            {
                ResultPotion = PotionGenerator.Instance.GetCreatedPotion(Ingredient1, Ingredient2);
            }
            else
            {
                ResultPotion = null;
            }
        }
    }

    public HerbData Ingredient2
    {
        get
        {
            return _ingredient2;
        }

        set
        {
            if (value != null && value == _ingredient1)
                return;

                _ingredient2 = value;
            if (value == null)
            {
                _ingredient2Sprite.color = new Color(1, 1, 1, 0);
                _ingredient2Sprite.sprite = null;
                _itemTooltipHelper2._item = null;
            }
            else
            {
                _ingredient2Sprite.color = new Color(1, 1, 1, 1);
                _ingredient2Sprite.sprite = value.SpriteImage;
                _itemTooltipHelper2._item = value;
            }
            if (Ingredient1 != null && value != null)
            {
                ResultPotion = PotionGenerator.Instance.GetCreatedPotion(Ingredient1, Ingredient2);
            }
            else
            {
                ResultPotion = null;
            }
        }
    }

    public Potion ResultPotion
    {
        get
        {
            return _resultPotion;
        }

        set
        {
            _resultPotion = value;
            if (value == null)
            {
                _resultPotionSprite.color = new Color(1, 1, 1, 0);
            }
            else
            {
                _resultPotionSprite.color = new Color(1, 1, 1, 1);
            }
            if (value != null)
            {
                _resultPotionSprite.sprite = value.SpriteImage;
                _itemTooltipHelper3._item = value;
            }
            else
            {
                _resultPotionSprite.sprite = null;
                _itemTooltipHelper3._item = null;
            }
        }
    }

    public void RemoveIngredient(int index)
    {
        if (index == 1)
        {
            Ingredient1 = null;
        }
        else
        {
            Ingredient2 = null;
        }
    }

    public void OnCraftPotion()
    {
        if (Ingredient1 == null || Ingredient2 == null)
            return;

        if (_potionInventory.PotionList.Contains(ResultPotion))
        {
            ResultPotion.Amount++;
        }
        else
        {
            ResultPotion.Amount++;
            _potionInventory.PotionList.Add(ResultPotion);
        }

        Ingredient1.Amount--;
        Ingredient2.Amount--;
        SpeechArea.Instance.SetSpeech("Me", "I crafted an " + ResultPotion.ToolTipText + "!");
        if (Ingredient1.Amount == 0)
        {
            Ingredient1 = null;
        }
        if (Ingredient2.Amount == 0)
        {
            Ingredient2 = null;
        }
        GetComponent<AudioSource>().PlayOneShot(_createPotionAudio);
    }
}
