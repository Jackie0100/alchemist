﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class SpeechArea : MonoBehaviour, IPointerClickHandler
{
    [SerializeField]
    Text _nameText;
    [SerializeField]
    Text _infoText;
    public static SpeechArea Instance { get; set; }

    private void Awake()
    {
        Instance = this;
        gameObject.SetActive(false);
    }

    public void SetSpeech(string name, string info)
    {
        _nameText.text = name;
        _infoText.text = info;
        gameObject.SetActive(true);
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        gameObject.SetActive(false);
    }
}
