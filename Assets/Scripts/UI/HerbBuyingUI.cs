﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(AudioSource))]
public class HerbBuyingUI : MonoBehaviour
{
    [SerializeField]
    public Image _resultHerbSprite;
    [SerializeField]
    AudioClip _buyHerbAudio;
    [SerializeField]
    ItemTooltipHelper _itemTooltipHelper;
    [SerializeField]
    FloatRef _money;

    HerbData _ingredient;

    public HerbData Ingredient
    {
        get
        {
            return _ingredient;
        }

        set
        {
            if (value == null)
            {
                _resultHerbSprite.color = new Color(1, 1, 1, 0);
            }
            else
            {
                _resultHerbSprite.color = new Color(1, 1, 1, 1);
            }
            if (value != null)
            {
                _resultHerbSprite.sprite = value.SpriteImage;
                _itemTooltipHelper._item = value;
            }
            else
            {
                _resultHerbSprite.sprite = null;
                _itemTooltipHelper._item = null;
            }
            _ingredient = value;
            _ingredient = value;
        }
    }

    public void BuyHerb()
    {
        if (_money.Value >= 250)
        {
            _ingredient.Amount++;
            _money.Value -= 250;
        }
        else
        {
            SpeechArea.Instance.SetSpeech("Me", "I do not have enough money!");
        }
    }
}
