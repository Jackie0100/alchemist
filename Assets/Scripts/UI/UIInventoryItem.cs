﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class UIInventoryItem : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    [SerializeField]
    Image _itemImage;
    [SerializeField]
    Text _amount;
    [SerializeField]
    Button _button;
    [SerializeField]
    IInventoryItem _item;
    [SerializeField]
    public CreatePotion _craftingUi;
    [SerializeField]
    public HerbBuyingUI _buyingUi;
    [SerializeField]
    public PotionSellingUI _sellingUi;

    public bool _keepZeroes;


    public IInventoryItem Item
    {
        get
        {
            return _item;
        }

        set
        {
            _item = value;
            _itemImage.sprite = _item.SpriteImage;
            _amount.text = _item.Amount.ToString();
        }
    }

    private void Update()
    {
        if (_item.Amount == 0 && !_keepZeroes)
        {
            Destroy(gameObject);
        }
        _amount.text = _item.Amount.ToString();
    }

    public void OnHerbItemClicked()
    {
        if (_craftingUi.Ingredient1 == null)
        {
            _craftingUi.Ingredient1 = (HerbData)_item;
        }
        else
        {
            _craftingUi.Ingredient2 = (HerbData)_item;
        }
    }

    public void OnPotionItemClicked()
    {
        _sellingUi.SellingPotion = (Potion)_item;
    }

    public void OnBuyHerbItemClicked()
    {
        _buyingUi.Ingredient = (HerbData)_item;
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        ToolTip.Instance.gameObject.SetActive(true);
        ToolTip.Instance.DisplayText(_item.ToolTipText);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        ToolTip.Instance.gameObject.SetActive(false);
    }
}