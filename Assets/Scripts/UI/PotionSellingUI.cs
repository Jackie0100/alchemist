﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class PotionSellingUI : MonoBehaviour
{
    [SerializeField]
    Image _npcImage;
    [SerializeField]
    public Image _resultPotionSprite;
    [SerializeField]
    PotionInventory _potionInventory;
    [SerializeField]
    AudioClip _sellPotionAudio;
    [SerializeField]
    ItemTooltipHelper _itemTooltipHelper1;
    [SerializeField]
    Text _npcInfoText;
    [SerializeField]
    bool _isSellingUi;
    [SerializeField]
    Text _selltestButtonText;
    [SerializeField]
    FloatRef _reputation;
    [SerializeField]
    FloatRef _money;

    List<NPC> AvailbleNpcs;
    
    int _npcIndex;

    private Potion sellingPotion;

    public Potion SellingPotion
    {
        get
        {
            return sellingPotion;
        }

        set
        {
            if (value == null)
            {
                _resultPotionSprite.color = new Color(1, 1, 1, 0);
            }
            else
            {
                _resultPotionSprite.color = new Color(1, 1, 1, 1);
            }
            if (value != null)
            {
                _resultPotionSprite.sprite = value.SpriteImage;
                _itemTooltipHelper1._item = value;
            }
            else
            {
                _resultPotionSprite.sprite = null;
                _itemTooltipHelper1._item = null;
            }
            sellingPotion = value;
        }
    }

    public int NpcIndex
    {
        get
        {
            return _npcIndex;
        }

        set
        {
            if (value >= AvailbleNpcs.Count)
                _npcIndex = 0;
            else if (value < 0)
                _npcIndex = AvailbleNpcs.Count - 1;
            else
                _npcIndex = value;

            _npcImage.sprite = AvailbleNpcs[_npcIndex].NpcImage;
            if (_isSellingUi)
            {
                if (_npcIndex == 0)
                {
                    _npcInfoText.text = AvailbleNpcs[_npcIndex].NpcName + "\n" + "I will trash this potion and remove it from my inventory!";
                    _selltestButtonText.text = "Trash Potion";
                }
                else
                {
                    _npcInfoText.text = AvailbleNpcs[_npcIndex].NpcName + "\n" + "I will pay " + AvailbleNpcs[_npcIndex].Payment + " for a potion, I need " + GetEffectNames(AvailbleNpcs[_npcIndex].AskedPotions) + "potions please!";
                    _selltestButtonText.text = "Sell Potion";
                }
            }
            else
            {
                if (_npcIndex == 0)
                {
                    _npcInfoText.text = AvailbleNpcs[_npcIndex].NpcName + "\n" + "I drink this potion and discover its effect, but it may also hurt me!";
                    _selltestButtonText.text = "Drink Potion";
                }
                else
                {
                    _npcInfoText.text = AvailbleNpcs[_npcIndex].NpcName + "\n" + "If you pay me " + AvailbleNpcs[_npcIndex].Payment + " I will let you test one potion on me!";
                    _selltestButtonText.text = "Test Potion";
                }
            }
        }
    }

    string GetEffectNames(List<HerbEffect> list)
    {
        string retstr = "";

        if (list == null)
        {
            return " ";
        }

        foreach (HerbEffect he in list)
        {
            retstr += he.Name + ", ";
        }

        return retstr;
    }

    private void OnEnable()
    {
        AvailbleNpcs = PotionGenerator.Instance.GenerateNPCs(_isSellingUi).Where(x => !x.HasBeenServed).ToList();
        NpcIndex = 0;
    }

    public void PrevNpc()
    {
        NpcIndex--;
    }

    public void NextNpc()
    {
        NpcIndex++;
    }

    public void TestPotion()
    {
        if (NpcIndex != 0)
        {
            if (_money.Value < AvailbleNpcs[NpcIndex].Payment)
            {
                SpeechArea.Instance.SetSpeech("Me", "I do not have enough money to pay this tester!");
                return;
            }
            if (SellingPotion.HerbEffect != null && SellingPotion.HerbEffect.EffectAlignment == EffectAlignment.Negative)
            {
                if (SellingPotion.HasPlaceboEffect)
                {
                    _reputation.Value -= ((1500 - AvailbleNpcs[NpcIndex].Payment) / 400.0f);
                }
                else
                {
                    _reputation.Value -= ((1500 - AvailbleNpcs[NpcIndex].Payment) / 200.0f);
                }
            }
            _money.Value -= AvailbleNpcs[NpcIndex].Payment;
            AvailbleNpcs[NpcIndex].HasBeenServed = true;
            AvailbleNpcs.Remove(AvailbleNpcs[NpcIndex]);
            NpcIndex--;
            if (sellingPotion.HerbEffect != null)
            {
                SpeechArea.Instance.SetSpeech("Me", "I discovered " + sellingPotion.Name + " and its effect of " + sellingPotion.HerbEffect.Name + "!");
            }
            else
            {
                SpeechArea.Instance.SetSpeech("Me", "I discovered " + sellingPotion.Name + "!");

            }
        }
        else
        {
            if (SellingPotion.HerbEffect != null && SellingPotion.HerbEffect.EffectAlignment == EffectAlignment.Negative)
            {
                PotionGenerator.Instance.AddUsageTime(8 * 60);
                SpeechArea.Instance.SetSpeech("Me", "After being passed out for multiple hours I realised I discovered " + sellingPotion.Name + " and its effect of " + sellingPotion.HerbEffect.Name + "!");
            }
            else
            {
                SpeechArea.Instance.SetSpeech("Me", "I discovered " + sellingPotion.Name + "!");
            }
        }

        sellingPotion.HasBenIdentified = true;
        foreach (HerbData hd in sellingPotion.Herbs)
        {
            if (hd.MajorEffect == sellingPotion.HerbEffect)
            {
                hd.HasDiscoveredMajorEffect = true;
            }
            else if (hd.MinorEffects[0] == sellingPotion.HerbEffect)
            {
                hd.HasDiscoveredMinorEffect1 = true;
            }
            else if (hd.MinorEffects[1] == sellingPotion.HerbEffect)
            {
                hd.HasDiscoveredMinorEffect2 = true;
            }
            else if (hd.MinorEffects[2] == sellingPotion.HerbEffect)
            {
                hd.HasDiscoveredMinorEffect3 = true;
            }
        }

        SellingPotion.Amount--;
        if (SellingPotion.Amount == 0)
        {
            SellingPotion = null;
        }
    }

    public void SellPotion()
    {
        if (NpcIndex != 0)
        {
            if (!AvailbleNpcs[NpcIndex].AskedPotions.Contains(SellingPotion.HerbEffect) && !SellingPotion.HasPlaceboEffect)
            {
                _reputation.Value -= (AvailbleNpcs[NpcIndex].Payment / 200.0f);
            }
            else
            {
                if (SellingPotion.HasPlaceboEffect)
                {
                    _reputation.Value += ((1000 - AvailbleNpcs[NpcIndex].Payment) / 400.0f);
                }
                else
                {
                    _reputation.Value += ((1000 - AvailbleNpcs[NpcIndex].Payment) / 200.0f);
                }
            }
            SpeechArea.Instance.SetSpeech("Me", "I sold " + sellingPotion.Name + " for " + AvailbleNpcs[NpcIndex].Payment + " gold!");
            _money.Value += AvailbleNpcs[NpcIndex].Payment;
            AvailbleNpcs[NpcIndex].HasBeenServed = true;
            AvailbleNpcs.Remove(AvailbleNpcs[NpcIndex]);
            NpcIndex--;
        }
        else
        {
            SpeechArea.Instance.SetSpeech("Me", "I threw away a " + sellingPotion.Name + "!");
            sellingPotion.Amount--;
        }


        SellingPotion.Amount--;
        if (SellingPotion.Amount == 0)
        {
            SellingPotion = null;
        }

    }
}
