﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ItemTooltipHelper : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public IInventoryItem _item;

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (_item == null)
            return;

        ToolTip.Instance.gameObject.SetActive(true);
        ToolTip.Instance.DisplayText(_item.ToolTipText);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        if (_item == null)
            return;

        ToolTip.Instance.gameObject.SetActive(false);
    }
}
