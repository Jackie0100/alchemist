﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HerbCraftingUI : MonoBehaviour
{
    [SerializeField]
    GameObject _itemUi;
    [SerializeField]
    HerbInventory _herbInventory;
    [SerializeField]
    CreatePotion _craftingUi;


    private void OnEnable()
    {
        foreach (var t in GetComponentsInChildren<UIInventoryItem>())
        {
            Destroy(t.gameObject);
        }

        GameObject go = null;
        foreach (var herb in _herbInventory.HerbList)
        {
            go = Instantiate(_itemUi, transform);
            go.GetComponent<UIInventoryItem>().Item = herb;
            go.GetComponent<UIInventoryItem>()._craftingUi = _craftingUi;

            go.GetComponent<Button>().onClick.AddListener(go.GetComponent<UIInventoryItem>().OnHerbItemClicked);
        }
    }
}
