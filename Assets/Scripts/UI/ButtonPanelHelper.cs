﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ButtonPanelHelper : MonoBehaviour
{
    [SerializeField]
    HerbInventory _herbInventory;
    [SerializeField]
    PotionInventory _potionInventory;
    [SerializeField]
    ItemInventory _itemInventory;

    [SerializeField]
    GameObject _searchForHerbsPanel;
    [SerializeField]
    GameObject _potionCraftingPanel;
    [SerializeField]
    GameObject _buyHerbPanel;
    [SerializeField]
    GameObject _browseMagicShopPanel;
    [SerializeField]
    GameObject _potionTestingPanel;
    [SerializeField]
    GameObject _potionSellingPanel;

    public void SearchForHerbsButtonClicked()
    {
        string speechstr = "I found ";
        foreach (var p in PotionGenerator.Instance.GetHerbs(2, 5))
        {
            speechstr += p.Name + ", ";
            if (_herbInventory.HerbList.Contains(p))
            {
                p.Amount++;
            }
            else
            {
                p.Amount++;
                _herbInventory.HerbList.Add(p);
            }
        }
        speechstr += "what a nice harvest of herbs!";
        SpeechArea.Instance.SetSpeech("Me", speechstr);
    }

    public void CraftPotionButtonClicked()
    {
        _potionCraftingPanel.SetActive(true);
    }

    public void BuyHerbsButtonClicked()
    {
        _buyHerbPanel.SetActive(true);
    }

    public void BrowseMagicShopButtonClicked()
    {
        _browseMagicShopPanel.SetActive(true);
    }

    public void TestPotionButtonClicked()
    {
        _potionTestingPanel.SetActive(true);
    }

    public void SellPotionButtonClicked()
    {
        _potionSellingPanel.SetActive(true);
    }
}
