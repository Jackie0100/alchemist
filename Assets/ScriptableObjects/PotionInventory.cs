﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "ScriptableObjects/Potion Inventory")]
public class PotionInventory : ScriptableObject
{
    [SerializeField]
    List<Potion> _potionList;

    public List<Potion> PotionList
    {
        get
        {
            return _potionList;
        }

        set
        {
            _potionList = value;
        }
    }
}
