﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "ScriptableObjects/Herb Inventory")]
public class HerbInventory : ScriptableObject
{
    [SerializeField]
    List<HerbData> _herbList;

    public List<HerbData> HerbList
    {
        get
        {
            return _herbList;
        }

        set
        {
            _herbList = value;
        }
    }
}
