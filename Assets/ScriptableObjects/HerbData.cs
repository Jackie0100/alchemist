﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "ScriptableObjects/HerbData")]
public class HerbData : ScriptableObject, IInventoryItem
{
    [SerializeField]
    string _name;
    [SerializeField]
    Sprite _herbSprite;
    [SerializeField]
    int _amount;
    [SerializeField]
    HerbEffect _majorEffect;
    [SerializeField]
    List<HerbEffect> _minorEffects;
    bool _hasDiscoveredMajorEffect;
    bool _hasDiscoveredMinorEffect1;
    bool _hasDiscoveredMinorEffect2;
    bool _hasDiscoveredMinorEffect3;

    public string Name
    {
        get
        {
            return _name;
        }
        set
        {
            _name = value;
        }
    }

    public Sprite HerbSprite
    {
        get
        {
            return _herbSprite;
        }
        private set
        {
            _herbSprite = value;
        }
    }

    public HerbEffect MajorEffect
    {
        get
        {
            return _majorEffect;
        }
        set
        {
            _majorEffect = value;
        }
    }

    public List<HerbEffect> MinorEffects
    {
        get
        {
            return _minorEffects;
        }
        set
        {
            _minorEffects = value;
        }
    }

    public int Amount
    {
        get
        {
            return _amount;
        }

        set
        {
            _amount = value;
        }
    }

    public Sprite SpriteImage
    {
        get
        {
            return HerbSprite;
        }
        set
        {
            HerbSprite = value;
        }
    }

    public string ToolTipText
    {
        get
        {
            return Name + "\nMajor effect: " + (_hasDiscoveredMajorEffect ? MajorEffect.Name : "Unknown") + "\n Minor effects: " + (_hasDiscoveredMinorEffect1 ? MinorEffects[0].Name : "Unknown") + ", " + (_hasDiscoveredMinorEffect2 ? MinorEffects[1].Name : "Unknown") + ", " + (_hasDiscoveredMinorEffect3 ? MinorEffects[2].Name : "Unknown");
        }
        set
        {
        }
    }

    public bool HasDiscoveredMajorEffect
    {
        get
        {
            return _hasDiscoveredMajorEffect;
        }

        set
        {
            _hasDiscoveredMajorEffect = value;
        }
    }

    public bool HasDiscoveredMinorEffect1
    {
        get
        {
            return _hasDiscoveredMinorEffect1;
        }

        set
        {
            _hasDiscoveredMinorEffect1 = value;
        }
    }

    public bool HasDiscoveredMinorEffect2
    {
        get
        {
            return _hasDiscoveredMinorEffect2;
        }

        set
        {
            _hasDiscoveredMinorEffect2 = value;
        }
    }

    public bool HasDiscoveredMinorEffect3
    {
        get
        {
            return _hasDiscoveredMinorEffect3;
        }

        set
        {
            _hasDiscoveredMinorEffect3 = value;
        }
    }
}
