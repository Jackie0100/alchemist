﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "ScriptableObjects/ItemInventory")]
public class ItemInventory : ScriptableObject
{
    [SerializeField]
    List<HerbData> _itemList;

    public List<HerbData> ItemList
    {
        get
        {
            return _itemList;
        }

        set
        {
            _itemList = value;
        }
    }
}
