﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "ScriptableObjects/HerbEffect")]
public class HerbEffect : ScriptableObject
{
    [SerializeField]
    string _name;
    [SerializeField]
    HerbEffect _oppesiteEffect;
    [SerializeField]
    EffectAlignment _effectAlignment;
    [SerializeField]
    bool _hasBeenDiscovered;
    [SerializeField]
    int _timesUsed;

    public int TimesUsed
    {
        get
        {
            return _timesUsed;
        }
        set
        {
            _timesUsed = value;
        }
    }

    public string Name
    {
        get
        {
            return _name;
        }
        private set
        {
            _name = value;
        }
    }

    public HerbEffect OppesiteEffect
    {
        get
        {
            return _oppesiteEffect;
        }
        private set
        {
            _oppesiteEffect = value;
        }
    }

    public EffectAlignment EffectAlignment
    {
        get
        {
            return _effectAlignment;
        }
        private set
        {
            _effectAlignment = value;
        }
    }

    public bool HasBeenDiscovered
    {
        get
        {
            return _hasBeenDiscovered;
        }
        private set
        {
            _hasBeenDiscovered = value;
        }
    }
}
