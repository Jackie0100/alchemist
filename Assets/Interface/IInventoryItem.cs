﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IInventoryItem
{
    string Name { get; set; }
    int Amount { get; set; }
    Sprite SpriteImage { get; set; }
    string ToolTipText { get; set; }
}
